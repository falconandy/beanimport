import csv
import datetime
import re
import pprint
import logging
from os import path

from dateutil.parser import parse

from beancount.core.number import D
from beancount.core.number import ZERO
from beancount.core.number import MISSING
from beancount.core import data
from beancount.core import account
from beancount.core import amount
from beancount.core import position
from beancount.core import inventory
from beancount.ingest import importer
from beancount.ingest import regression

class Importer(importer.ImporterProtocol):

    def __init__(self,currency = "USD",
                 account = {'4422':'Assets:Checking',
                 '1142':'Liabilities:CreditCard',},
                 checking_nums = ['4422'],
                 cc_nums = ['1142'],
                 ):

        """
        c_**** are accounts for different files numbers f.ex:
        Chase****_Activity_20180416.CSV '****' - numbers
        """
        self.currency = currency
        self.account = account
        self.checking_nums = checking_nums
        self.cc_nums = cc_nums


    def identify(self, file):
        """ Check if the file fills in Chase template
        """
        return re.search(r'Chase\d\d\d\d_Activity_\d\d\d\d\d\d\d\d.CSV',
                        path.basename(file.name))

    def file_name(self, file):
        return 'chase.{}'.format(path.basename(file.name))

    def file_account(self, file):
        key_num = re.search(r'\d\d\d\d', file.name).group()
        if key_num not in (self.checking_nums + self.cc_nums):
            logging.error('undefined Chase**** - %s number'%key_num)
        return self.account[key_num]

    def file_date(self, file):
        """ Parse date
        """

        date_nums = re.search(r'Chase\d\d\d\d_Activity_(\d\d\d\d\d\d\d\d).CSV',
                             path.basename(file.name))

        return datetime.datetime.strptime(date_nums.group(1),
                                          '%Y%m%d').date()
    def extract(self, file, existing_entries=None):
        """Open and parse .csv file, build Transactions, return entries
        """
        entries = []
        with open(file.name) as csvfile:
            reader = csv.DictReader(csvfile)
            index = 0
            balance=0
            for index, row in enumerate(reader):
                key_num = re.search(r'\d\d\d\d', file.name).group()
                meta = data.new_metadata(file.name, index)
                file_account = self.account[key_num]

                if key_num in self.cc_nums:
                    trans_date = parse(row['Trans Date']).date()
                    description = row['Description']
                    m_amount = amount.Amount(D(row['Amount']), self.currency)
                    txn = data.Transaction(meta, trans_date, self.FLAG, None,
                                           description, data.EMPTY_SET, data.EMPTY_SET,[
                            data.Posting(file_account, m_amount,  None, None, None, None), ]
                    )

                elif key_num in self.checking_nums:
                    posting_date = parse(row['Posting Date']).date()
                    description = row['Description']
                    m_amount = amount.Amount(D(row['Amount']), self.currency)
                    if not balance:
                        balance = row['Balance']
                        bal_post_date = posting_date
                    txn = data.Transaction(meta, posting_date, self.FLAG, None,
                                           description, data.EMPTY_SET, data.EMPTY_SET,[
                        data.Posting(file_account, m_amount, None, None, None, None), ])

                else:
                    logging.error('not referenced number Chase(****) - %s'%key_num)
                    continue


                entries.append(txn)

        if balance:
            entries.append(
                data.Balance(
                    meta, bal_post_date + datetime.timedelta(days=1),
                    file_account,
                             amount.Amount(D(balance), self.currency),
                             None, None)
            )

        return entries

def test():
    # Create an importer instance for running the regression tests.
    importer = Importer()
    yield from regression.compare_sample_files(importer, __file__)