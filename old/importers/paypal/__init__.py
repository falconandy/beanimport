import csv
import datetime
import re
import pprint
import logging
from os import path

from dateutil.parser import parse

from beancount.core.number import D
from beancount.core.number import ZERO
from beancount.core.number import MISSING
from beancount.core import data
from beancount.core import account
from beancount.core import amount
from beancount.core import position
from beancount.core import inventory
from beancount.ingest import importer
from beancount.ingest import regression

class Importer(importer.ImporterProtocol):

    def __init__(self,
                 accounts ={
                     'PP': "Assets:Paypal",},
                 ):
        self.account = accounts
        self.fieldnames = ['Date', 'Time', 'TimeZone', 'Name', 'Type', 'Status', 'Currency', 'Gross',
                      'Fee', 'Net', 'From Email Address', 'To Email Address', 'Transaction ID',
                      'Item Title', 'Reference Txn ID', 'Receipt ID', 'Balance']


    def identify(self, file):
        return re.search(r'Download\w\w.CSV', path.basename(file.name))

    def file_name(self, file):
        date = self.file_date(file).strftime('%m%d%Y')
        return 'paypal-{}.{}'.format(date, path.basename(file.name))

    def file_account(self, file):
        key = re.search(r'Download(\w\w).CSV', path.basename(file.name)).group(1)
        return self.account[key]

    def file_date(self, file):
        with open(file.name) as csvfile:
            reader = csv.DictReader(csvfile, dialect='excel', fieldnames=self.fieldnames)
            for row in reader:
                if not row['Date']:
                    continue
                last_date = row['Date']
        date = datetime.datetime.strptime(last_date, '%m/%d/%Y').date()
        return date

    def extract(self, file, existing_entries=None):
        entries = []

        with open(file.name) as csvfile:
            reader = csv.DictReader(csvfile, dialect='excel', fieldnames=self.fieldnames)
            key = re.search(r'Download(\w\w).CSV', path.basename(file.name)).group(1)
            file_account = self.account[key]
            for index, row in enumerate(reader):
                # skip blank lines
                if not row['Date']:
                    continue
                # some bad code to skip if header was read
                if row['Time'] == 'Time':
                    continue
                meta = data.new_metadata(file.name, index)
                trans_date = parse(row['Date']).date()
                payee = row['Name']
                balance, currency = row['Balance'], row['Currency']

                # choose payee
                if row['Item Title']:
                    description = row['Item Title']
                else:
                    description = row['Type']

                amount_m = amount.Amount(D(row['Net']), row['Currency'])


                txn = data.Transaction(meta, trans_date, self.FLAG, payee,
                                           description, data.EMPTY_SET, data.EMPTY_SET,[
                        data.Posting(file_account, amount_m, None, None, None, None), ])
                entries.append(txn)

            # add balance
            entries.append(
                data.Balance(
                    meta, trans_date + datetime.timedelta(days=1),
                    file_account,
                    amount.Amount(D(balance), currency),
                    None, None)
            )

            return entries

def test():
    # nosetests -s -v --all-modules importers/
    # Create an importer instance for running the regression tests.
    importer = Importer()
    yield from regression.compare_sample_files(importer, __file__)