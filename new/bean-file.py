import argparse
import os
import re


def remove_date_prefixes():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', dest='output_dir', default='')
    args, _ = parser.parse_known_args()

    renames = []
    date_prefix_re = re.compile(r'^\d{4}-\d{2}-\d{2}\.(.*)$')
    for (dirpath, dirnames, filenames) in os.walk(args.output_dir):
        for filename in filenames:
            match = date_prefix_re.match(filename)
            if match:
                renames.append((os.path.join(dirpath, filename), os.path.join(dirpath, match[1])))

    for old_name, new_name in renames:
        if os.path.exists(new_name):
            print('can''t rename "%s" to "%s" - already exists' % (old_name, new_name))
        else:
            os.replace(old_name, new_name)


from beancount.ingest.file import main

main()
remove_date_prefixes()
