import calendar
import collections
import copy
import datetime
import re
from csv import reader as CsvReader

import dateutil.parser
from beancount.core import data
from beancount.core.amount import Amount
from beancount.core.number import D, ZERO
from beancount.ingest.importers import csv
from beancount.ingest.importers.csv import Col, normalize_config
from aenum import extend_enum
from beancount.parser.printer import EntryPrinter
from beancount.utils.date_utils import parse_date_liberally

MAX_AMOUNTS = 20
MAX_SUPPLS = 10
MAX_NARRATIONS = 10

for i in range(MAX_AMOUNTS):
    extend_enum(Col, "AMOUNT" + str(i), "[AMOUNT" + str(i) + "]")

for i in range(MAX_SUPPLS):
    extend_enum(Col, "SUPPL" + str(i), "[SUPPL" + str(i) + "]")

extend_enum(Col, "NARRATION0", "[NARRATION0]")
for i in range(4, MAX_NARRATIONS):
    extend_enum(Col, "NARRATION" + str(i), "[NARRATION" + str(i) + "]")

Rule = collections.namedtuple('rules', ['txnregex', 'narration', 'suppl', 'accounts', 'amounts', 'aggregation'])
Entry = collections.namedtuple('Entry', ['rule', 'txn'])

EntryPrinter.PlainText = lambda _, entry, oss: oss.write(entry.text)


class PlainText:
    def __init__(self, date, text):
        self.date = date
        self.text = text
        self.meta = {'filename': 'dummy', 'lineno': -1}


data.ALL_DIRECTIVES = tuple(data.ALL_DIRECTIVES + (PlainText,))


class EntryGroup:
    def __init__(self, year, month):
        self.year = year
        self.month = month
        self.header = []
        self.entries = []
        self.footer = []

    def fill_header(self, msplit):
        if not msplit:
            return
        self.header.append(PlainText(datetime.date(self.year, self.month, 1), '***** ' + calendar.month_abbr[self.month]))

    def fill_footer(self, iconfig, msplit, account, currency):
        if not msplit:
            return

        if Col.BALANCE in iconfig:
            txn = self.entries[-1].txn
            balance = txn.meta.get('balance', None)
            if balance is not None:
                meta = data.new_metadata(txn.meta.get('filename', ''), txn.meta.get('lineno', -1))
                _, day_count = calendar.monthrange(self.year, self.month)
                next_period_start = datetime.date(self.year, self.month, day_count) + datetime.timedelta(days=1)
                self.footer.append(data.Balance(meta, next_period_start,
                                                account, Amount(balance, currency),
                                                None, None))

    def aggregate(self, rules, mslpit):
        if not mslpit:
            return

        primary_accounts = rules[-1].accounts
        _, day_count = calendar.monthrange(self.year, self.month)
        period_end = datetime.date(self.year, self.month, day_count)
        aggregations = []
        for rule in rules:
            if rule.aggregation and rule.aggregation not in aggregations:
                aggregations.append(rule.aggregation)

        for i, aggregation in enumerate(aggregations):
            aggregation_rules = [rule for rule in rules if rule.aggregation == aggregation]
            aggregation_txns = [entry.txn for entry in self.entries if entry.rule.aggregation == aggregation]
            if len(aggregation_txns) == 0:
                continue
            self.entries = [entry for entry in self.entries if entry.rule.aggregation != aggregation]

            postings = collections.OrderedDict()
            for r in aggregation_rules:
                for account in r.accounts:
                    if account not in primary_accounts:
                        postings[account] = None
            for account in primary_accounts:
                postings[account] = None

            for txn in aggregation_txns:
                for posting in txn.postings:
                    if postings.get(posting.account) is None:
                        postings[posting.account] = posting
                    else:
                        aggregated_posting = postings[posting.account]
                        postings[posting.account] = data.Posting(posting.account,
                                                                 data.Amount(aggregated_posting.units.number + posting.units.number, aggregated_posting.units.currency),
                                                                 None, None, None, None)

            meta = copy.copy(aggregation_txns[-1].meta)
            meta['lineno'] = 1000000 + i
            aggregation_txn = data.Transaction(meta, period_end, aggregation_txns[-1].flag, None,
                                               "%s %s %d" % (aggregation, calendar.month_abbr[self.month], self.year),
                                               data.EMPTY_SET, data.EMPTY_SET,
                                               list([p for p in postings.values() if p is not None]))
            self.entries.append(Entry(aggregation_rules[0], aggregation_txn))


class CsvImporter(csv.Importer):
    def __init__(self, config, account, narration=None, currency='USD', fnregex=None, rules=None, msplit='off', aggregation=None,
                 skip_last_lines=0, ignore_number_chars='', **kwargs):
        matchers = kwargs.setdefault('matchers', [])
        if fnregex:
            matchers.append(('filename', fnregex))

        if not isinstance(account, list):
            account = [account]

        primary_accounts = [account[0] for account in account]
        primary_amounts = [account[1] for account in account]
        self.primary_account = primary_accounts[-1]

        primary_narrations = []
        if narration is not None:
            primary_narrations = narration if isinstance(narration, list) else [narration]

        self.rules = []
        self.negative_amount_cols = set()
        self.fill_rules(config, rules, primary_accounts, primary_amounts, aggregation, primary_narrations)

        self.msplit = msplit.lower() == 'on' if msplit else False
        self.skip_last_lines = skip_last_lines
        self.ignore_number_chars = ignore_number_chars

        super().__init__(config, self.primary_account, currency, **kwargs)
        del self.remap['mime']

    def fill_rules(self, config, rules, primary_accounts, primary_amounts, primary_aggregation, primary_narrations):
        narration_cols = {}
        suppl_cols = {}
        amount_cols = {}

        def fill_amounts(amounts):
            for i, amount in enumerate(amounts):
                if amount not in amount_cols:
                    amount_col = Col['AMOUNT' + str(len(amount_cols))]
                    if amount.startswith("-"):
                        self.negative_amount_cols.add(amount_col)
                    config[amount_col] = amount[1:] if amount.startswith("-") or amount.startswith("+") else amount
                    amount_cols[amount] = amount_col
                amounts[i] = amount_cols[amount]

        def fill_narration(narration):
            for i, narration_name in enumerate(narration):
                if narration_name not in narration_cols:
                    narration_col = Col['NARRATION' + str(len(narration_cols))]
                    config[narration_col] = narration_name
                    narration_cols[narration_name] = narration_col
                narration[i] = narration_cols[narration_name]

        def fill_suppl(suppl):
            for i, suppl_name in enumerate(suppl):
                if suppl_name not in suppl_cols:
                    suppl_col = Col['SUPPL' + str(len(suppl_cols))]
                    config[suppl_col] = suppl_name
                    suppl_cols[suppl_name] = suppl_col
                suppl[i] = suppl_cols[suppl_name]

        fill_amounts(primary_amounts)
        fill_narration(primary_narrations)

        if rules is not None:
            for rule in rules:
                narration = None
                if 'narration' in rule and rule['narration'] != '':
                    narration = rule['narration'] if isinstance(rule['narration'], list) else [rule['narration']]
                    fill_narration(narration)

                suppl = []
                if 'suppl' in rule and rule['suppl']:
                    suppl = rule['suppl'] if isinstance(rule['suppl'], list) else [rule['suppl']]
                    fill_suppl(suppl)

                if 'account' in rule:
                    account = rule['account'] if isinstance(rule['account'], list) else [rule['account']]
                    accounts = [account[0] for account in account]
                    amounts = [account[1] for account in account]
                    fill_amounts(amounts)
                else:
                    accounts = []
                    amounts = []

                accounts.extend(primary_accounts)
                amounts.extend(primary_amounts)

                self.rules.append(Rule(txnregex=re.compile(rule['txnregex']), narration=narration if narration is not None else primary_narrations,
                                       suppl=suppl, accounts=accounts, amounts=amounts, aggregation=rule.get('aggregation', None)))

        self.rules.append(Rule(txnregex=None, narration=primary_narrations, suppl=[], accounts=primary_accounts, amounts=primary_amounts, aggregation=primary_aggregation))

    def extract(self, file, existing_entries=None):
        entry_groups = {}

        # Normalize the configuration to fetch by index.
        # Skip garbage lines before sniffing the header
        head = file.head()
        for _ in range(self.skip_lines):
            head = head[head.find('\n') + 1:]
        head = head[:head.find('\n')]
        iconfig, has_header = normalize_config(
            self.config, head, self.csv_dialect, 0)

        reader = iter(CsvReader(open(file.name), dialect=self.csv_dialect))

        # Skip garbage lines
        for _ in range(self.skip_lines):
            next(reader)

        # Skip header, if one was detected.
        if has_header:
            next(reader)

        def get(row, ftype):
            try:
                return row[iconfig[ftype]] if ftype in iconfig else None
            except IndexError:  # FIXME: this should not happen
                return None

        # Parse all the transactions.
        first_row = last_row = None
        rows = list(reader)
        row_count = 0
        for index, row in enumerate(rows, 1):
            if not row:
                continue
            if row[0].startswith('#'):
                continue

            row_count += 1
            if len(rows) - row_count == self.skip_last_lines - 1:
                break

            # If debugging, print out the rows.
            if self.debug:
                print(row)

            if first_row is None:
                first_row = row
            last_row = row

            # Extract the data we need from the row, based on the configuration.
            date = get(row, Col.DATE)
            txn_date = get(row, Col.TXN_DATE)
            txn_time = get(row, Col.TXN_TIME)

            payee = get(row, Col.PAYEE)
            if payee:
                payee = payee.strip()

            tag = get(row, Col.TAG)
            tags = {tag} if tag is not None else data.EMPTY_SET

            last4 = get(row, Col.LAST4)

            balance = get(row, Col.BALANCE)

            # Create a transaction
            meta = data.new_metadata(file.name, index)
            if txn_date is not None:
                meta['date'] = parse_date_liberally(txn_date,
                                                    self.dateutil_kwds)
            if txn_time is not None:
                meta['time'] = str(dateutil.parser.parse(txn_time).time())
            if balance is not None:
                meta['balance'] = self.get_number(balance)
            if last4:
                last4_friendly = self.last4_map.get(last4.strip())
                meta['card'] = last4_friendly if last4_friendly else last4
            date = parse_date_liberally(date, self.dateutil_kwds)

            txn_rule, narration = self.find_txn_rule(get, row)
            txn = data.Transaction(meta, date, self.FLAG, payee, narration,
                                   tags, data.EMPTY_SET, [])
            amounts = [self.get_amount(iconfig, row, amount_col) for amount_col in txn_rule.amounts]

            # Skip empty transactions
            if all(amount is None for amount in amounts):
                continue

            for i, amount in enumerate(amounts):
                if amount is None:
                    continue
                units = Amount(amount, self.currency)
                txn.postings.append(
                    data.Posting(txn_rule.accounts[i], units, None, None, None, None))

            # Attach the other posting(s) to the transaction.
            if isinstance(self.categorizer, collections.abc.Callable):
                txn = self.categorizer(txn)

            entry_period = date.year, date.month
            if entry_period not in entry_groups:
                entry_groups[entry_period] = EntryGroup(*entry_period)
            entry_groups[entry_period].entries.append(Entry(rule=txn_rule, txn=txn))

        # Figure out if the file is in ascending or descending order.
        first_date = parse_date_liberally(get(first_row, Col.DATE), self.dateutil_kwds)
        last_date = parse_date_liberally(get(last_row, Col.DATE), self.dateutil_kwds)
        is_ascending = first_date < last_date

        for entry_group in entry_groups.values():
            # Reverse the list if the file is in descending order
            if not is_ascending:
                entry_group.entries = list(reversed(entry_group.entries))

            entry_group.fill_header(self.msplit)
            entry_group.fill_footer(iconfig, self.msplit, self.primary_account, self.currency)
            entry_group.aggregate(self.rules, self.msplit)

        entries = []
        for period in sorted(entry_groups.keys()):
            entry_group = entry_groups[period]
            entries.extend(entry_group.header)
            entries.extend(entry.txn for entry in entry_group.entries)
            entries.extend(entry_group.footer)

        # Add a balance entry if possible
        if not self.msplit and Col.BALANCE in iconfig and entries:
            entry = entries[-1]
            date = entry.date + datetime.timedelta(days=1)
            balance = entry.meta.get('balance', None)
            if balance is not None:
                meta = data.new_metadata(file.name, index)
                entries.append(
                    data.Balance(meta, date,
                                 self.primary_account, Amount(balance, self.currency),
                                 None, None))

        # Remove the 'balance' metadata.
        for entry in entries:
            entry.meta.pop('balance', None)

        return entries

    def find_txn_rule(self, get, row):
        for rule in self.rules:
            fields = filter(None, [get(row, field) for field in rule.narration])
            narration = self.narration_sep.join(field.strip() for field in fields)

            fields = filter(None, [get(row, field) for field in rule.suppl])
            suppl = self.narration_sep.join(field.strip() for field in fields)
            narration_suppl = self.narration_sep.join(filter(None, (narration, suppl)))

            if rule.txnregex is None or rule.txnregex.match(narration_suppl):
                return rule, narration
        return None, ''

    def get_amount(self, iconfig, row, column):
        amount = None
        if column in iconfig:
            amount = row[iconfig[column]]

        amount_number = self.get_number(amount)
        is_zero_amount = amount is not None and amount_number == ZERO
        if is_zero_amount:
            return None

        if amount is not None and column in self.negative_amount_cols:
            amount_number = -amount_number

        return amount_number if amount else None

    def get_number(self, value):
        if isinstance(value, str):
            for char in self.ignore_number_chars:
                value = value.replace(char, '')
        return D(value)
